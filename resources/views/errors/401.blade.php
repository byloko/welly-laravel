@extends('errors::minimal')

@section('title')
   Welly - Unauthorized
@endsection

@section('code')
   
     <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5">
                    <div class="form-input-content text-center error-page">
                        <h1 class="error-text font-weight-bold">401</h1>
                        <h4><i class="fa fa-times-circle text-danger"></i> Unauthorized</h4>
                        <p>You do not have permission to view this resource</p> 
						<div>
                            <a class="btn btn-primary" href="{{ url('/') }}">Back to Home</a>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
