<!DOCTYPE html>
<html lang="en" class="h-100">


<!-- Mirrored from welly.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Dec 2020 16:38:48 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welly - Hospital Bootstrap Admin Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;family=Roboto:wght@100;300;400;500;700;900&amp;display=swap" rel="stylesheet">
</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
									<div class="text-center mb-3">
										<a href="{{ url('/') }}">
                                            <img src="{{ asset('images/logo-full.png') }}" alt="">
                                        </a>
									</div>
                                    <h4 class="text-center mb-4 text-white">Sign in your account</h4>
                                    <form action="javascript:void(0)" method="post" id="login_us">
                                        <div class="alert alert-danger d-none" id="msg_div">
                                            <span id="res_message"></span>
                                       </div>
                                        @if(Session::has('message'))
                                            <p id="msg_div_hide" class="alert alert-success">{{ Session::get('message') }}</p>
                                        @endif
                                        
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>Email</strong></label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>Password</strong></label>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                               <div class="custom-control custom-checkbox ml-1 text-white">
													<input type="checkbox" class="custom-control-input" id="basic_checkbox_1">
													<label class="custom-control-label" for="basic_checkbox_1">Remember my preference</label>
												</div>
                                            </div>
                                            <div class="form-group">
                                                <a class="text-white" onclick="getRedirectPageForgotPassword()">Forgot Password?</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" id="send_login_form" class="btn bg-white text-primary btn-block">Sign Me In</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p class="text-white">Don't have an account? <a class="text-white" onclick="getRedirectPageRegister()">Sign up</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('vendor/global/global.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/deznav-init.js') }}"></script>
    <script>
        function getRedirectPageForgotPassword(){
            // alert("Alert");
            window.location.href = '{{ route('page.forgot.password') }}'
        }

        function getRedirectPageRegister(){
            // alert("Alert");
            window.location.href = '{{ route('page.register') }}'
        }

        setTimeout(function(){
            $('#msg_div_hide').remove();
        }, 5000);


        $('#send_login_form').click(function(e){
            e.preventDefault();
            // alert("Alert");
            /* Ajax Request header setup */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            });
            /* Submit form data using ajax */
            $.ajax({
                url: "{{ route('page.login.check') }}",
                method: 'post',
                data: $('#login_us').serialize(),
                success: function(response) {
                    console.log(response);
                    if(response == 1)
                    {
                        window.location.replace('{{route('admin.dashboard')}}');
                    }
                    else if(response == 3)
                    {
                        $('#res_message').show();
                        $('#res_message').html("Username or Password  Incorrect. Please Check..!");
                        $('#msg_div').removeClass('d-none');
                    }
                }
            });
        });
        
    </script>
</body>


<!-- Mirrored from welly.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Dec 2020 16:38:49 GMT -->
</html>