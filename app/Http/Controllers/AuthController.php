<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public function page_login(Request $request)
    {
      return view('auth.page_login');
    }

    public function page_forgot_password()
    {
      return view('auth.page_forgot_password');
    }

    public function page_register()
    {
      return view('auth.page_register');
    }

    public function page_register_store(Request $request)
    {

      $result = new User;
      $result->name = $request->name;
      $result->email = $request->email;
      $result->password = Hash::make($request->password);
      $result->save();
    
      if($result){
        $arr = array('msg' => 'Register successfully!', 'status' => true);
      }
      Session::flash('message', 'Registered successfully. please login..!'); 
      return Response()->json($arr);
    }

    public function page_login_check(Request $request){
      $email = $request->email;
      $pass = $request->password;
      if(auth()->attempt(array('email' => $email, 'password' => $pass)))
      {
        return response()->json([ [1] ]);
      }else{
        return response()->json([ [3] ]);
      }
    }

    public function page_logout(Request $request){
      Auth::logout();
	  	return redirect(route('page.login'));
    }

    public function page_forgot_check(Request $request){
      // dd($request->all());
      
    }
    
}
