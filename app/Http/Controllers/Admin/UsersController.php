<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index(Request $request){
        return view('admin.users.index');
    }

    // public function list(){
    //     $query = User::select('name', 'email');
    //     dd($query);
    //     return datatables($query)->make(true);
    // }
    public function list(){
        $categories = User::OrderByDESC('id')->get();
        $allData = array();
        $SrNo = 1;
        foreach($categories as $category) {
            $rowRes = new \StdClass();
            $rowRes->SrNo = $SrNo++;
            $rowRes->ID = $category['id'];
            $rowRes->Name = $category['name'];
            $rowRes->EmailID = $category['email'];
            $rowRes->Action = '<div class="d-flex">
                <a href="'.route('admin.users.edit', $category['id']).'" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                <a href="'.route('admin.users.delete',$category['id']).'" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a></div>';
            $allData[] = $rowRes;
        }

        echo json_encode(array('data'=>$allData));
    }

    public function edit($id){
        $data['getRecord'] = User::find($id);
        return view('admin.users.edit', $data);
    }

    public function delete($id){
        echo $id;
        die();
    }

    public function create(){
        // echo "Hello";die();
        return view('admin.users.add');
    }

}
