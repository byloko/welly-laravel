<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo $id;
        die();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id;
        die();
    }

    public function list(){
        $getRecord = Order::get();
        $alldata = array();
        $SrNo = 1;
        foreach($getRecord as $value){
            $rowRes = new \stdClass();
            $rowRes->SrNo = $SrNo++;
            $rowRes->ID = $value->id;
            $rowRes->Username = ($value->user != null ? $value->user->name : '');    
            $rowRes->Ordername = $value->order_name;
            $rowRes->Action = '<div class="d-flex">
            <a href="'.route('admin.order.edit', $value->id).'" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
            <button class="btn btn-primary shadow btn-xs sharp mr-1 edit_open_modal" value="'.$value->id.'"><i class="fa fa-pencil"></i></button>
            <button class="btn btn-danger shadow btn-xs sharp destroy_open_modal"><i class="fa fa-trash"></i></button>&nbsp;
            <a href="'.route('admin.order.destroy', $value->id).'" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
          

            </div>';
            $alldata[] = $rowRes;
        }
        echo json_encode(array('data'=>$alldata));
    }

    public function edit_modal($id){
        return Order::findOrFail($id);
    }
}
