<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('page-login', [AuthController::class, 'page_login'])->name('page.login');
Route::post('page-login-check', [AuthController::class, 'page_login_check'])->name('page.login.check');
Route::get('page-forgot-password', [AuthController::class, 'page_forgot_password'])->name('page.forgot.password');
Route::post('page-forgot-check', [AuthController::class, 'page_forgot_check'])->name('page.forgot.check');
Route::get('page-register', [AuthController::class, 'page_register'])->name('page.register');
Route::post('page-register-store', [AuthController::class, 'page_register_store'])->name('page.register.store');
Route::get('page-logout', [AuthController::class, 'page_logout'])->name('page.logout');

Route::get('admin/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

Route::get('admin/users', [UsersController::class, 'index'])->name('admin.users');
Route::get('admin/users/list', [UsersController::class, 'list'])->name('admin.users.list');
Route::get('admin/users/edit/{id}', [UsersController::class, 'edit'])->name('admin.users.edit');
Route::get('admin/users/delete/{id}', [UsersController::class, 'delete'])->name('admin.users.delete');
Route::get('admin/users/create', [UsersController::class, 'create'])->name('admin.users.create');

Route::get('admin/order', [OrderController::class, 'index'])->name('admin.order');
Route::get('admin/order/list', [OrderController::class, 'list'])->name('admin.order.list');
Route::get('admin/order/edit/{id}', [OrderController::class, 'edit'])->name('admin.order.edit');
Route::get('admin/order/destroy/{id}', [OrderController::class, 'destroy'])->name('admin.order.destroy');
Route::get('admin/order/{id}/edit_modal', [OrderController::class, 'edit_modal']);


